#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <aruco.h>

#include <iostream>
#include <string>

#define ESC_KEY 27
using namespace cv;
using namespace std;


int main( int argc, char** argv )

{

	// creation d �un detecteur de marqueurs
	aruco::MarkerDetector myDetector;

	// listqe de marqueurs : sera remplie par ArUco
	vector <aruco::Marker> markers ;

    VideoCapture cap(0); // open the default camera
    if(!cap.isOpened())  // check if we succeeded
        return -1;

    // The current image
// retrieved from the video capture
// or read from a file
Mat image;
// The image converted into grayscale (see if we use it)
Mat gray_image;

// Creating a window to display the images
string windowName = "ArUco";
namedWindow(windowName, CV_WINDOW_AUTOSIZE);


	int key = 0;
	bool gray=false;
	while(key != ESC_KEY) {
		// Getting the new image from the camera
		cap >> image;

		// detect ion
		myDetector.detect (image,markers) ;

		// on affiche le resultat de la detection sur une image
		for (unsigned int i=0; i<markers.size(); i++) {
			//cout << markers[i];
			markers[i].draw(image,Scalar(0,0,255),2);
		}

		if (key=='g')
		{
			gray=!gray;
		}
		if (gray)
		{
			cvtColor(image,gray_image,CV_BGR2GRAY);
			imshow(windowName,gray_image);
			bool gray=true;
		}
		else 
		{
			// Showing the image in the window
			imshow(windowName,image);
			bool gray=false;
		}

		// On recupere le code de la touche pressee par l�utilisateur
		key = waitKey(1);
	}

// Destroying the windows
destroyWindow(windowName);

// Releasing the video capture
cap.release();


return EXIT_SUCCESS;
}